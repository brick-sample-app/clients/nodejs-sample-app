//import libraries
const express = require('express')
const app = express()
const port = 3000
const request = require('request')
const cors = require('cors')
const fs = require('fs')
const { exit } = require('process')

//access the json data from url in post or get, eg. to retrieve the user-access-token
app.use(express.json())

//access static file
app.use(express.static('public'))
app.use('/css',express.static(__dirname + 'public/css'))
app.use(cors())
// app.use('/js',express.static(__dirname + 'public/js'))

//set view folder for ejs file
app.set('views','./views')
//set engine ejs so we dont need to use extension in string name
app.set('view engine','ejs')

//read clientId and clientSecret from key.txt
var arrayasd = fs.readFileSync('readme.txt').toString().replace("clientId=","").replace("clientSecret=","").replaceAll("\r","").replaceAll(" ","").split("\n");
if(arrayasd[0] == "" || arrayasd[1] == ""){
    console.log("Please insert clientID and clientSecret in key.txt")
    process.exit(0)
}

//dictionary for public-token and user-access-token
var dict = {
    "public_token" : "",
    "user_token" : ""
}

//dictionary for transactions detail
var transaction_dict = {
    "date" : [],
    "description":[],
    "status" : [],
    "amount" : []
}

//dictionary for user account list in one user ID
var user_account ={
    "accountHolder" :[],
    "accountNumber" :[],
    "accountID":[],
    "balance":{
        "available" : "",
        "current" : ""
    }
}

//dicitonary for user details for specific user account
var user_detail ={
    "accountHolder" :"",
    "accountNumber" :"",
    "accountID":"",
    "balance":{
        "available" : "",
        "current" : ""
    }
}

//boolean to check for Jenius Bank or not (MFA)
var boolean = {
    "isJenius" : false
}

//boolean to store jenius bank details
var jenius ={
    "jeniusTransactionDetail" : [],
    "jeniusAccountDetail" : []
    
}

//base link for BRICK API
var base_link = 'https://sandbox.onebrick.io/v1/'

//variable to get current date
var date = new Date()
var year = date.getFullYear()
var month = ("0" +date.getMonth()+1).slice(-2)
var day = ("0" +date.getDate()).slice(-2)

//first function to be called when program is executed
app.get('/',(req,res)=>{
    //render index.ejs
    res.render('index')
})

//post function triggered when index form is submitted
app.post('/accessbrick', function(req,res){
    //callback authentication function
    authentication(function(body){
        //redirect to BRICK API(call brick widget)
        res.redirect(base_link+'index?'+"accessToken="+body+"&redirect_url="+"http://localhost:3000/result")
    })
    
})

//post function to BRICK WIDGET
app.post('/result', function(req,res){
    //reset dictionaries value
    dict["user_token"] = ""
    jenius['jeniusTransactionDetail'] = []
    jenius['jeniusAccountDetail'] = []
    //variable to retrieve user_access_token
    var object =req.body
    //count the length of JSON response, if more than one then user is using Jenius Bank
    var count = Object.keys(object).length;
    //condition to check if user is using Jenius Bank or not
    if(count > 2){
        //when transactions is not null
        if(object['transactions'] != null){
            //set boolean if jenius bank is used
            boolean['isJenius'] = true
            //read the transaction response in JSON
            var temp = JSON.parse(object['transactions'])
            //iterate the tranasction list and push to an array
            for (var i =0; i<temp.length;i++){
                jenius['jeniusTransactionDetail'].push(temp[i])
            }
        }
        if(object['accounts'] != null){
            //read the transaction response in JSON
            var temp2 = JSON.parse(object['accounts'])
            //iterate the tranasction list and push to an array
            for (var i =0; i<temp2.length;i++){
                jenius['jeniusAccountDetail'].push(temp2[i])
            }
        }
    }
    //retrieve user-access-token from the POST response
    var user_token = object['accessToken']
    dict['user_token'] = user_token
    //variable to generate sessionId
    var sessionId =''
    var chars ='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    for (var i = 20; i > 0; --i){
        sessionId += chars[Math.floor(Math.random() * chars.length)]
    }
    //sent post value
    res.send('http://localhost:3000/result?sessionId='+sessionId)
})

//get function to BRICK WIDGET
app.get('/result', function(req,res){
    //condition when user-access-token is available
    if(dict['user_token'] != ""){
        //condition when user is using Jenius Bank
        if(boolean['isJenius'] == true){
            //call getTransactionJenius to retrieve all the transaction details
            getTransactionJenius(function(transaction_body){
                getUserDetailJenius(function(detail_body){
                    var transaction = transaction_body
                    var userDetail = detail_body
                    //render result page and send over the transaction details and date
                    res.render('result', {userDetail:userDetail,transaction:transaction, date: year+'-'+month+'-'+day})
                })
            })
        //user using other bank than Jenius Bank
        }else{
            //call getTransaction to retrieve all the transaction details
            getTransaction(function(transaction_body){
                //call getUserDetail to retrieve all user details
                getUserDetail(function(detail_body){
                    //variable to store the function return values
                    var transaction = transaction_body
                    var userDetail = detail_body
                    //render result page and send over the transaction details, user details, and date
                    res.render('result', {userDetail:userDetail, transaction:transaction, date: year+'-'+month+'-'+day})
                })
            })
        }
    //condition when user-access-token is not available
    }else{
        res.render('error')
    }
})

// function to retrieve public access token
function authentication(callback){
    //reset public token
    dict["public_token"]=""
    //variable for clientID:clientSecret 
    var account = arrayasd.join(":")
    var temp = new Buffer(account)
    //buffer to convert client ID : client secret with base64
    var authorize = "Basic "+ temp.toString('base64')
    
    //headers for HTTP
    var headers = {
        'Content-type':'application/json',
        'Authorization':authorize
    }
    //link parameters
    var link = {
        method : 'GET',
        strictSSL : false,
        url : base_link+"auth/token",
        headers : headers
    }
    //request function
    request(link, function (error, res, body){
        //if not error
        if(!error){
            //retrieve public-access-token and store it to the dictionary['public_token']
            var object = JSON.parse(body)
            var public_token = object['data']['access_token']
            dict["public_token"] = public_token
            callback(public_token)
        }else{
            console.log(error)
        }
    })
}

//function to get the account list
function getUserAccount(callback){
    //reset user account dictionary
    user_account['accountHolder'] = []
    user_account['accountNumber'] = []
    user_account['accountID'] = []
    user_account['balance']['available'] = ""
    user_account['balance']['current'] = ""
    //get user token from dictionary which is retrieved from BRICK widget
    var user_token = dict['user_token']
    //variable headers for http
    var headers = {
        'Content-type':'application/json',
        'Authorization':'Bearer ' + user_token
    }
    //link parameters
    var link = {
        method : 'GET',
        strictSSL : false,
        url : base_link+"account/list",
        headers : headers
    }
    //request function
    request(link, function (error, res, body){
        if(!error){
            //retrieve JSON reponse
            var object = JSON.parse(body)
            //store account list
            var account_list = []
            account_list.push(object['data'][0])
            //if account list is empty, means we are using Mock Bank
            if(account_list.length == 0){
                user_account['accountHolder'].push("JohnDoe")
                user_account['accountNumber'].push("9870675789")
                user_account['accountID'].push("S8vzhyw3owDtYDOB87va==")
                user_account['balance']['available']="100,987"
                user_account['balance']['current']="100,987" 
            }
            //else if account list exist
            else{
                //iterate through all the account list and store into user_account dictionary
                for (var i = 0; i < account_list.length;i++){
                    user_account['accountHolder'].push(account_list[i]['accountHolder'])
                    user_account['accountNumber'].push(account_list[i]['accountNumber'])
                    user_account['accountID'].push(account_list[i]['accountId'])
                    user_account['balance']['available']=account_list[i]['balances']['available'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    user_account['balance']['current']=account_list[i]['balances']['current'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
            }
            //callback the user_account dictionary
            callback(user_account)
        }else{
            console.log(error)
        }
    })
}

//function to get account details
function getUserDetail(callback){
    //reset user_detail in dictionary
    user_detail['accountHolder'] = ""
    user_detail['accountNumber'] = ""
    user_detail['accountID'] = ""
    user_detail['balance']['available'] = ""
    user_detail['balance']['current'] = ""
    //get the user-access-token from dictionary
    var user_token = dict['user_token']
    //call getUserAccount function to get the accountID
    getUserAccount(function(account_body){
        //variable headers for HTTP
        var headers = {
            'Content-type':'application/json',
            'Authorization':'Bearer ' + user_token
        }
        //variable for accountID
        var accountID = account_body['accountID']
        //link parameter
        var link = {
            method : 'GET',
            strictSSL : false,
            url : base_link+"account/detail?accountId="+accountID,
            headers : headers
        }
        //request function
        request(link, function (error, res, body){
            if(!error){
                //variable to store json response
                var object = JSON.parse(body)
                var account_detail = object['data']
                //populate user_detail dictionary with json reponses
                user_detail['accountHolder'] = account_detail['accountHolder']
                user_detail['accountNumber'] = account_detail['accountNumber']
                user_detail['accountID'] = account_detail['accountId']
                user_detail['balance']['available']=account_detail['balances']['available'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                user_detail['balance']['current']=account_detail['balances']['current'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                //return the dictionary
                callback(user_detail)
            }else{
                console.log(error)
            }
        })
    })
    
}

//function to retrieve transactions
function getTransaction(callback){
    //reset the transaction dictionary
    transaction_dict['date']=[]
    transaction_dict['description']=[]
    transaction_dict['status']=[]
    transaction_dict['amount']=[]
    //get the user-access-token
    var user_token = dict['user_token']
    //variable headers for HTTP
    var headers = {
        'Content-type':'application/json',
        'Authorization':'Bearer ' + user_token
    }
    //link parameter
    var link = {
        method : 'GET',
        strictSSL : false,
        url : base_link+"transaction/list?from=2021-01-01&to=2021-12-31",
        headers : headers
    }
    //request function
    request(link, function (error, res, body){
        if(!error){
            //variable to store JSON response
            var object = JSON.parse(body)
            var tran_list = object['data']
            //populate transaction dictionary with JSON response
            for (var i = 0; i < tran_list.length;i++){
                transaction_dict['date'].push(tran_list[i]['date'])
                transaction_dict['description'].push(tran_list[i]['description'])
                transaction_dict['status'].push(tran_list[i]['status'])
                // transaction_dict['amount'].push(tran_list[i]['amount'].replace(/\B(?=(\d{3})+(?!\d))/g, ","))
                transaction_dict['amount'].push(tran_list[i]['amount'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
            }
            //return transaction dictionary
            callback(transaction_dict)
        }else{
            console.log(error)
        }
    })
}

//function get Transaction if user uses Jenius Bank
function getTransactionJenius(callback){
    //reset transasction dictionary
    transaction_dict['date']=[]
    transaction_dict['description']=[]
    transaction_dict['status']=[]
    transaction_dict['amount']=[]
    //populate transaction dictionary with response from POST method in Brick API
    for (var i = 0; i < jenius['jeniusTransactionDetail'].length;i++){
        transaction_dict['date'].push(jenius['jeniusTransactionDetail'][i]['date'])
        transaction_dict['description'].push(jenius['jeniusTransactionDetail'][i]['description'])
        transaction_dict['status'].push(jenius['jeniusTransactionDetail'][i]['status'])
        transaction_dict['amount'].push(jenius['jeniusTransactionDetail'][i]['amount'])
    }
    //return transaction dictionary
    callback(transaction_dict)
}

//function getUserDetail if user uses Jenius Bank
function getUserDetailJenius(callback){
    //reset user_detail dictionary
    user_detail['accountHolder'] = ""
    user_detail['accountNumber'] = ""
    user_detail['accountID'] = ""
    user_detail['balance']['available'] = ""
    user_detail['balance']['current'] = ""
    //populate user_detail dictionary with response from POST method in Brick API
    for (var i = 0; i < jenius['jeniusAccountDetail'].length;i++){
        user_detail['accountHolder'] = jenius['jeniusAccountDetail'][0]['accountHolder']
        user_detail['accountNumber'] = jenius['jeniusAccountDetail'][0]['accountNumber']
        user_detail['accountID'] = jenius['jeniusAccountDetail'][0]['accountId']
        user_detail['balance']['available']= (jenius['jeniusAccountDetail'][0]['balances']['available']).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        user_detail['balance']['current']= (jenius['jeniusAccountDetail'][0]['balances']['current']).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
    //return user_detail dictionary
    callback(user_detail)
}

//function listen to port
app.listen(port, function(){
    console.info(`Listening to port ${port}`)
})